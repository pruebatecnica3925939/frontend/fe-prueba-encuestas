import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormEncuestaComponent } from './components/form-encuesta/form-encuesta.component';
import { ResultadoEncuestaComponent } from './components/resultado-encuesta/resultado-encuesta.component'; 
import { MenuEncuestaComponent } from './components/menu-encuesta/menu-encuesta.component';  

const routes: Routes = [
  {
    path:'', component: MenuEncuestaComponent
  },
  {
    path:'encuesta', component: FormEncuestaComponent
  },
  {
    path:'resultado', component: ResultadoEncuestaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
