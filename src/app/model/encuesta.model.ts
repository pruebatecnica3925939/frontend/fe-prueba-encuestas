export interface EncuestaReq {
    correo: string;
    estiloMusical: string;
}

export interface Estadistica {
    count: number;
    estiloMusical: string;
}

export interface EstiloMusical {
    value: string;
    viewValue: string;
  }
