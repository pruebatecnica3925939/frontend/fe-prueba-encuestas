import { Injectable } from '@angular/core';
import { HttpClient,HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EncuestaReq,Estadistica } from '../model/encuesta.model';
import { environment } from 'src/environments/environment'; 

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {

  private apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }

  listarEstadistica(): Observable<Estadistica[]> {
    return this.httpClient.get<Estadistica[]>(
      `${this.apiUrl}encuesta/estadistica`,
    );
  }

  guardarEncuesta(request:EncuestaReq): Observable<HttpResponse<void>> {
    return this.httpClient.post<void>(
      `${this.apiUrl}encuesta/`,
      request,
      { observe: 'response' }
    );
  }

  obtenerEstilosMusicales(): Observable<string[]> {
    return this.httpClient.get<string[]>(`${this.apiUrl}encuesta/estilos`);
  }



}
