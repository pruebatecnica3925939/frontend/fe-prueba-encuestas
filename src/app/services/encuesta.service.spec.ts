import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { of } from "rxjs";
import { EncuestaService } from './encuesta.service';
import { estilosEsperadosMock,estadisticaReqMock,encuestaReqMock } from '../mock/encuesta';

describe('EncuestaService', () => {
  let serviceGet: EncuestaService;
  let servicePost: EncuestaService; 
 
  let httpClientSpyGet: { get: jasmine.Spy };
  let httpClientSpyPost: { post: jasmine.Spy };


  beforeEach(() => {
    httpClientSpyGet = jasmine.createSpyObj("HttpClient", ["get"]);
    httpClientSpyPost= jasmine.createSpyObj("HttpClient", ["post"]);
    
    serviceGet = new EncuestaService(httpClientSpyGet as any);
    servicePost = new EncuestaService(httpClientSpyPost as any);

    TestBed.configureTestingModule({});
  });

  it('Obtener los estilos musicales', (done) => {
    
    httpClientSpyGet.get.and.returnValue(of(estilosEsperadosMock));
    
    serviceGet.obtenerEstilosMusicales().subscribe(estilos => {
      expect(estilos).toEqual(estilosEsperadosMock);
      done();
    });
  });

  it('Listar la estadistica', (done) => {
    
    httpClientSpyGet.get.and.returnValue(of(estadisticaReqMock));
    
    serviceGet.listarEstadistica().subscribe(estilos => {
      expect(estilos).toEqual(estadisticaReqMock);
      done();
    });
  });

  it("Guardar encuesta", (done) => {
    const respuestaEsperada = new HttpResponse<void>({ status: 200 }); // especificando <void>
    httpClientSpyPost.post.and.returnValue(of(respuestaEsperada));
  
    servicePost.guardarEncuesta(encuestaReqMock).subscribe(response => {
      expect(response).toEqual(respuestaEsperada);
      done();
    });
  });

});
