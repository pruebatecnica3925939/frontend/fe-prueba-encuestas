import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; 
import { NgApexchartsModule } from "ng-apexcharts";
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormEncuestaComponent } from './components/form-encuesta/form-encuesta.component';
import { ResultadoEncuestaComponent } from './components/resultado-encuesta/resultado-encuesta.component';
import { MenuEncuestaComponent } from './components/menu-encuesta/menu-encuesta.component';


@NgModule({
  declarations: [
    AppComponent,
    FormEncuestaComponent,
    ResultadoEncuestaComponent,
    MenuEncuestaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgApexchartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
