import { EncuestaReq, EstiloMusical, Estadistica } from '../model/encuesta.model'; 

export const encuestaReqMock: EncuestaReq = 
    {
      correo: 'correo@correo.cl',
      estiloMusical:'rock'
    }

export const estadisticaReqMock: Estadistica[] =
[
    {
        count:12,
        estiloMusical:'Rock'
    },
    {
        count:12,
        estiloMusical:'Blues'
    }

]

export const estilosEsperadosMock: string[] = ['Rock', 'Pop'];

