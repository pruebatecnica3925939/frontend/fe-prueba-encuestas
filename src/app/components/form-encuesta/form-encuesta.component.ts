import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { NgForm,  FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { EncuestaReq,EstiloMusical } from 'src/app/model/encuesta.model';
import { EncuestaService } from 'src/app/services/encuesta.service';

@Component({
  selector: 'app-form-encuesta',
  templateUrl: './form-encuesta.component.html',
  styleUrls: ['./form-encuesta.component.css']
})



export class FormEncuestaComponent {
  encuestaForm: FormGroup;
  private subs: Subscription[] = [];
  estilosMusicales: EstiloMusical[] = [];
 
  constructor(private encuestaervice : EncuestaService, private fb: FormBuilder, private router: Router) {
    this.encuestaForm = this.fb.group({
      correo: ['', [Validators.required, Validators.email]],
      estiloMusical: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.listarEstilos()
  }
  
  guardarEncuesta(request: EncuestaReq) {
    this.subs.push(
      this.encuestaervice.guardarEncuesta(request).subscribe({
        next: (resp) => {
          if (resp.status == 200) {
            this.successAlert()
            this.router.navigate(['resultado']);
          }
        },
        error: (error) => {
          console.log(error.error.message)
          this.errorAlert(error.error.message); 
        }
      })
    )
    }

    listarEstilos(){
      this.encuestaervice.obtenerEstilosMusicales().subscribe(response => {
        this.estilosMusicales = response.map(estilo => {
          return { value: estilo, viewValue: estilo };
        });
      }, error => {
        this.errorAlert('Error al obtener los estilos musicales'); 
      });
    }

  onSubmit(form: NgForm) {
    console.log('Formulario enviado:', form.value);
    
    let request: EncuestaReq = {
      correo: form.value.email,
      estiloMusical: form.value.estilosMusicales  
    }
    this.guardarEncuesta(request);
  }

  successAlert(){
    Swal.fire({
      title: 'Éxito!',
      text: 'Encuesta enviada exitosamente.',
      icon: 'success',
      confirmButtonText: 'OK'
    });
    
    }

    errorAlert(errorMessage: string) {
      Swal.fire({
        title: 'Error!',
        text: errorMessage,
        icon: 'error',
        confirmButtonText: 'OK'
      });
    }
}

