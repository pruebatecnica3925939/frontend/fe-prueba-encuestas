import { Component, OnInit } from '@angular/core';
import * as ApexCharts from 'apexcharts';
import { Subscription } from 'rxjs';
import { EncuestaService } from 'src/app/services/encuesta.service';
@Component({
  selector: 'app-resultado-encuesta',
  templateUrl: './resultado-encuesta.component.html',
  styleUrls: ['./resultado-encuesta.component.css']
})
export class ResultadoEncuestaComponent implements OnInit{
  private subs: Subscription[] = [];

  constructor(private encuestaervice : EncuestaService) {
    
  }

  ngOnInit() {
    this.encuestaervice.listarEstadistica().subscribe(
      data => {
        const categories = data.map(item => item.estiloMusical);
        const seriesData = data.map(item => item.count);

        const options: ApexCharts.ApexOptions = {
          series: [{
            name: 'Votos',
            data: seriesData,
          }],
          chart: {
            type: 'bar',
            height: 350,
          },
          title: {
            text: 'Votos Estilos musicales',
          },
          xaxis: {
            categories: categories,
          },
        };

        const chart = new ApexCharts(document.getElementById('chart'), options);
        chart.render();
      },
      error => {
        console.error('Error al obtener los datos', error);
      }
    );
  }
}


