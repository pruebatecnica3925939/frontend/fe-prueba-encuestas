# fe-prueba-encuestas

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) versión 16.2.4.

Es un componente frontend que interactua con una API para el registro de encuestas, ademas de entregar
un grafico en relación a esta.

Se levanta como una aplicación angular:

Instala las dependencias ejecutando `npm install`

Ejecuta `ng serve` para un servidor de desarrollo. Navega a `http://localhost:4200/`. La aplicación se recargará automáticamente.

Asegurate de tener levantado en en tu entorno local, el repositorio backend para que todo funcione correctamente:

 `https://gitlab.com/pruebatecnica3925939/backend/ms-prueba-encuesta/-/tree/master?ref_type=heads `

Este esta configurado en la URL `http://localhost:8080/`


## Ejecución de Pruebas Unitarias

Ejecuta `ng test` para ejecutar las pruebas unitarias a través de [Karma](https://karma-runner.github.io).


## Autor
-Darío Montiel
